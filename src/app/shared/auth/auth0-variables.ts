interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'GrnEg4ppKBVHpkQOxVULvjAUHih0UUgK',
  domain: 'minima.eu.auth0.com',
  callbackURL: 'http://localhost:8569/callback'
};
