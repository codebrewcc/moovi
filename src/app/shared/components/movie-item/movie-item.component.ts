import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

export class Genre {
  id: number;
  name: string;
}

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.scss']
})

export class MovieItemComponent implements OnInit {

  genres: Genre[];

  constructor(private _route: ActivatedRoute) {
    this.genres = this._route.snapshot.data['genres'].genres;
  }

  getGenres(ids) {
    let ret = "";
    for (let i = 0; i < ids.length; i++) {
      this.genres.forEach((genre) => {
        if (genre.id === ids[i]) {
          ret += genre.name + (i == (ids.length - 1) ? "" : " | ");
        }
      });
    }

    return ret;
  }

  ngOnInit() {
    // console.info(this.genres);
  }

  @Input() data: string;

}
