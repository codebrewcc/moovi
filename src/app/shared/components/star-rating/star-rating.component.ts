import { Component, OnInit, Input } from '@angular/core';
// import { SpawnSyncOptionsWithStringEncoding } from 'child_process';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html'
  // styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit {

  setRating: number;

  constructor() { }

  ngOnInit() {
    this.setRating = 3;
  }

  @Input() rating: string;


}
