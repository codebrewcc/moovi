import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MenuService } from './menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuComponent {

  // @HostBinding('class.is-open')
  // isOpen = false;

  // constructor(
  //   private menuService: MenuService
  // ) { }

  // ngOnInit() {
  //   this.menuService.change.subscribe(isOpen => {
  //     this.isOpen = isOpen;
  //   });
  // }

  constructor() { }

  @Input() isOpen: boolean = false;
  @Output() toggledChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  toggleMenu() {
    this.isOpen = !this.isOpen;
    this.toggledChange.emit(this.isOpen);
  }
}

// https://medium.com/dailyjs/3-ways-to-communicate-between-angular-components-a1e3f3304ecb
// https://medium.com/@foolishneo/understanding-input-output-and-eventemitter-in-angular-c1aeb9fff594
