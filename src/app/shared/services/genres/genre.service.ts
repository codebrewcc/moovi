import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Genre } from './genre.model';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable()
export class GenreService {

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  constructor(private http: HttpClient) { }

  getGenres(): Observable<any> {
    return this.http.get('https://api.minimainc.com/v1/moovi/genres').pipe(
      map(response => {  // NOTE: response is of type SomeType
        return response;
      }),
      catchError(error => {
        return Observable.throw(error);
      })
    )
  }
}
