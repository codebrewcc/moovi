import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
// import { Employee } from '../models/employee.model';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { GenreService } from './genre.service';
import { Genre } from './genre.model';

@Injectable()
// Implement the Resolve interface, as we are implementing a route resolve guard
// Resolve interface supports generics, so specify the type of data that this
// resolver returns using the generic parameter
export class GenresResolverService implements Resolve<Genre[]> {
  // Inject the employeee service as we need it to retrieve employee data
  constructor(private genreService: GenreService) {
  // constructor() {
  }
  // Resolve interface contains the following one method for which we need to
  // provide implementation. This method calls EmployeeService & returns employee data
  resolve(route: ActivatedRouteSnapshot) {
    return this.genreService.getGenres();
    // return [];
  }
}
