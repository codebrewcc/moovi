export class Genre {
  id: number;
  name: string;
  checked: boolean;
}
