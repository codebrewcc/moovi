import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './modules/home/home.component';

import { ROUTES } from './app.routes';
import { GenreService } from './shared/services/genres/genre.service';
import { GenresResolverService } from './shared/services/genres/genre.resolver.service';

import { AuthService } from './shared/auth/auth.service';
import { CallbackComponent } from './modules/callback/callback.component';
import { MenuComponent } from './shared/components/menu/menu.component';
import { DiscoverComponent, GenreFilter, RatingFilter } from './modules/discover/discover.component';
import { StarRatingComponent } from './shared/components/star-rating/star-rating.component';
import { MovieItemComponent } from './shared/components/movie-item/movie-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CallbackComponent,
    MenuComponent,
    DiscoverComponent,
    StarRatingComponent,
    MovieItemComponent,
    GenreFilter,
    RatingFilter
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [AuthService, GenreService, GenresResolverService],
  bootstrap: [AppComponent]
})
export class AppModule { }
