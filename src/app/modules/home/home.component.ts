import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../shared/auth/auth.service';
import { Http, Response} from '@angular/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  data: string;
  loading: boolean;
  name: string = "John Doe";

  constructor(public auth: AuthService, private http: Http) { }

  ngOnInit() {
  }

  makeRequest(): void {
    this.loading = true;
    this.http.request('http://localhost:3689/v1/moovi').subscribe((res: Response) => {
      console.info(res);
      this.data = res.json();
      this.loading = false;
    });
  }

}
