import { Component, OnInit, Input, Pipe, PipeTransform } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response } from '@angular/http';
import { Genre } from '../../shared/services/genres/genre.model'

const findOne = (haystack, arr) => {
  return arr.some((v) => {
    return haystack.indexOf(v) >= 0;
  });
};

@Pipe({
  name: 'genreFilter',
  pure: false
})
export class GenreFilter implements PipeTransform {
  transform(items: any[], filter: number[]): any {
    if (filter && !filter.length) {
      return items;
    } else if (filter && filter.length) {
      // filter items array, items which match and return true will be
      // kept, false will be filtered out
      return items.filter(item => findOne(item.genre_ids, filter));
    }
    return items;
  }
}

@Pipe({
  name: 'ratingFilter',
  pure: false
})
export class RatingFilter implements PipeTransform {
  transform(items: any[], filter: number): any {
    if (filter && !filter) {
      return items;
    } else if (filter) {
      // filter items array, items which match and return true will be
      // kept, false will be filtered out
      return items.filter(item => item.vote_average >= filter);
    }
    return items;
  }
}

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss']
})
export class DiscoverComponent implements OnInit {

  data: string;
  genres: Genre[];
  selectedGenres: number[];
  selectedRating: number;
  showFilters: boolean = false;


  constructor(private router: Router, private _route: ActivatedRoute, private http: Http) {
    this.genres = this._route.snapshot.data['genres'].genres;
    this._route.queryParams.subscribe(params => {
      this.selectedRating = params.rating ? parseInt(params.rating) : 3;
      this.selectedGenres = params.genres ? JSON.parse(params.genres) : [];
    });
  }

  changeRoute() {
    this.router.navigate(['/app/showing'], { queryParams: { page: 1 } });
    // this.router.navigateByUrl(...)
  }

  updateSelected() {
    this.selectedGenres = this.genres
      .filter(opt => opt.checked)
      .map(opt => opt.id)

    this.router.navigate(['/app/showing'], { queryParams: { genres: JSON.stringify(this.selectedGenres), rating: this.selectedRating } });
  }

  updateSelectedRating(rating) {
    this.selectedRating = rating;
    this.router.navigate(['/app/showing'], { queryParams: { genres: JSON.stringify(this.selectedGenres), rating: rating } });
  }

  sortItems(a, b) {
    return b.popularity - a.popularity
  }

  ngOnInit() {
    //checking selected genres via params
    this.selectedGenres.forEach((selectedGenre: number) => {
      let itemIndex = this.genres.findIndex(item => item.id == selectedGenre);
      this.genres[itemIndex]["checked"] = true;
    });

    this.http.request('https://api.minimainc.com/v1/moovi/upcoming').subscribe((res: Response) => {
      this.data = res.json();
    });
  }

  @Input() apiResource: string;
  @Input() appRoute: string;

}
