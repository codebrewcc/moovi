import { Routes } from '@angular/router';
import { HomeComponent } from './modules/home/home.component';
import { CallbackComponent } from './modules/callback/callback.component';
import { DiscoverComponent } from './modules/discover/discover.component';
import { GenresResolverService } from './shared/services/genres/genre.resolver.service';

export const ROUTES: Routes = [
  // {
  //   path: '',
  //   component: HomeComponent
  // },
  {
    path: 'callback',
    component: CallbackComponent
  },
  {
    path: 'app',
    resolve: {
       /**
     * Since genres will be used by a number of different components and routes,
     * it is resolved once in this parent state in order to be easily accessed
     */
      genres: GenresResolverService
    },
    children: [
      {
        path: 'showing',
        component: DiscoverComponent,
      },
      {
        path: 'movies',
        component: DiscoverComponent,
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'app/showing'
  }
];
