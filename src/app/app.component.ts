import { Component } from '@angular/core';
import { AuthService } from './shared/auth/auth.service';
import { Http, Response} from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  toggle: boolean = false;
  toggleMenu: boolean = false;
  test: string = "test";

  constructor(public auth: AuthService, private http: Http) {
    auth.handleAuthentication();
    // this.toggle = false;
    // this.menu = false;
  }

  handleChange(isMenuOpen) {
    this.toggleMenu = isMenuOpen;
  }

  getSearch(){
    this.http.request('http://localhost:3689/v1/moovi/search').subscribe((res: Response) => {
      console.info(res.json());
      // this.data = res.json();
    });
  }


}
