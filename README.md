# Moovi Angular 6 App
This is an app exhibiting Angular 5 framework usage and utilizing the TheMovieDB API, found [here](https://www.themoviedb.org/documentation/api?language=en-US).

* NOTE: TheMovieDB API is consumed via a second API, https://api.minimainc.com/moovi, this was done with the intent of authenticating a user before the actual resource is consumed.

![TheMovieDB](https://www.themoviedb.org/assets/1/v4/logos/293x302-powered-by-square-blue-ee05c47ab249273a6f9f1dcafec63daba386ca30544567629deb1809395d8516.png)

## Requirements
For development, you will only need Node.js on your environment.

### Node

[Node](http://nodejs.org/) is really easy to install & now include [Angular CLI](https://cli.angular.io/).
You should be able to run the following commands after the installation procedure
below.

    $ node --version
    v8.11.2

    $ npm --version
    v5.6.0

    $ ng --version
    Angular CLI: 1.7.3

## Installation

* $ git clone git clone https://iestyn02@bitbucket.org/PixlLtd/moovi.git
* $ cd moovi
* $ npm install

## Build & Serve

* $ npm start
* $ Go to http://localhost:8569/ on your browser

## Demo

* https://moovi.minimainc.com/

## License

MIT

## TODO
* Enforce User authentication, API resource that consumes TheMovieDB API is currently public
* Search functionality
* Utilize "discover" component for other routes, i.e. movies, tv
* Lazy load app once user is authenticated
* Tests
* To add comments in code

